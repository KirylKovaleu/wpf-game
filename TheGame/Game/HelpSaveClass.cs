﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.IO;

namespace Game
{
    public static class HelpSaveClass
    {
        private static object _lock = new object();
        public static void Save<T>(this ObservableCollection<T> listQuestionViewModel)where T:class
        {
            lock (_lock)
            {
                var path = string.Format("../../{0}.json",typeof(T).Name);

                using (StreamWriter sw = new StreamWriter(path, false))
                {
                    sw.Write(Serialize(listQuestionViewModel));
                }
            }
        }

        private static string Serialize<T>(T listQuestionViewModel)
        {
            string result = JsonConvert.SerializeObject(listQuestionViewModel);
            return result;
        }

        public static ObservableCollection<T> GetAllQuestions<T>(this ObservableCollection<T> listQuestionViewModel)
        {
            var result = new ObservableCollection<T>();
            var path = string.Format("../../{0}.json", typeof(T).Name);
            if (File.Exists(path))
            {
                using (StreamReader sr = File.OpenText(path))
                {
                    string data = sr.ReadToEnd();
                    if (!string.IsNullOrEmpty(data))
                    {
                        result = Deserialize<ObservableCollection<T>>(data);
                    }
                }
            }

            return result;
        }

        private static T Deserialize<T>(string value)
        {
            T result = JsonConvert.DeserializeObject<T>(value);
            return result;
        }
    }
}
