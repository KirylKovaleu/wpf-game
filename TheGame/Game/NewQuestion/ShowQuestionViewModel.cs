﻿using Caliburn.Micro;
using Game.Models;
using Microsoft.Win32;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Game.NewQuestion
{
    public class ShowQuestionViewModel:Screen
    {

        private QuestionViewModel _model;
        private string _answerForPlayer;
        private int _countQuestion;
        private string _startText;
        private readonly IWindowManager _windowManager;
        public delegate void addDelegate(char c);
        private addDelegate deleg;
        private Thread _thread;

        public ShowQuestionViewModel(QuestionViewModel model, string str, IWindowManager windowManager)
        {
            _thread = new Thread(new ParameterizedThreadStart(StringHelper));
            _thread.Start(str);
            _model = model;
            _answerForPlayer = null;
            _windowManager = windowManager;
        }

        public string AnswerForPlayer
        {
            get { return _answerForPlayer; }
            set
            {
                _answerForPlayer = value;
                NotifyOfPropertyChange(() => AnswerForPlayer);
            }
        }
        public string StartText
        {
            get { return _startText; }
            set
            {
                _startText = value;
                NotifyOfPropertyChange(() => StartText);
            }
        }
        public QuestionViewModel Model
        {
            get { return _model; }
            set
            {
                _model = value;
                NotifyOfPropertyChange(() => Model);
            }
        }

        public void Result()
        {
            var answer = new List<RsponseViewModel>();
            answer.Add(_model.PossibleAnswer1);
            answer.Add(_model.PossibleAnswer2);
            answer.Add(_model.PossibleAnswer3);
            answer.Add(_model.PossibleAnswer4);

            var result = answer.Find(x => x.IsCheck == true);

            if (result != null)
            {
                if (result.Text.Equals(_model.AnswerTrue))
                {
                    result.IsCheck = false;
                    _countQuestion++;
                    TryClose(true);
                }
                else
                {
                    result.IsCheck = false;
                    _countQuestion = 0;
                    ResponseToUser();
                    TryClose(false);
                }
            }
        }

        public void StringHelper(object str)
        {
            deleg += addString;

            foreach (var s in (string)str)
            {
                Thread.Sleep(40);
                Application.Current.Dispatcher.Invoke(deleg, s);
            }

            void addString(char c)
            {
                StartText += c;
            }

        }

        public void ResponseToUser()
        {
            var driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Url = "https://www.google.by/";

            IWebElement searchField = driver.FindElement(By.XPath(".//*[@id='lst-ib']"));
            searchField.Clear();
            searchField.SendKeys(_model.Questionn);
            searchField = driver.FindElement(By.Id("_fZl"));
            searchField.Click();
        }
    }
}
