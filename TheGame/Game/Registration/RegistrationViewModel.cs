﻿using Caliburn.Micro;
using Game.Models;
using System.Collections.ObjectModel;

namespace Game.Registration
{
    public class RegistrationViewModel : Screen
    {
        private UserViewModel _userModel;
        private ObservableCollection<UserViewModel> _userList;
        private string _password;
        private string _error;

        public RegistrationViewModel()
        {
            _userModel = new UserViewModel();
            _userList = _userList.GetAllQuestions();
            _error = null;
        }

        public string Error
        {
            get { return _error; }
            set
            {
                _error = value;
                NotifyOfPropertyChange(() => Error);
            }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; NotifyOfPropertyChange(() => Password); }
        }
        public UserViewModel UserModel
        {
            get { return _userModel; }
            set
            {
                _userModel = value;
                NotifyOfPropertyChange(() => UserModel);
            }
        }

        public void Registration()
        {
            if (_userModel != null)
            {
                if (_userModel.Age != 0 && _userModel.login != null && _userModel.Password != null)
                {
                    if (_userModel.Password == Password)
                    {
                        _userModel.UserRole = new RoleViewModel { Role = Role.User };
                        _userList.Add(_userModel);
                        _userList.Save();

                        TryClose(true);
                    }
                    else
                    {
                        _error = "Введенные пароли не совпадают";
                        Refresh();
                    }
                }
                else
                {
                    _error = "Заполните все поля";
                    Refresh();
                }
            }
        }
    }
}
