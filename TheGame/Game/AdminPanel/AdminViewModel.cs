﻿using Caliburn.Micro;
using Game.Add;
using Game.Models;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace Game.AdminPanel
{
    public class AdminViewModel : Screen
    {
        private readonly IWindowManager _windowManager;
        private ObservableCollection<QuestionViewModel> _listQuestionViewModel;
        private ObservableCollection<UserViewModel> _userList;
        private Func<ObservableCollection<QuestionViewModel>, AddViewModel> _addQuestion;

        public AdminViewModel(IWindowManager windowManager,
            Func<ObservableCollection<QuestionViewModel>, AddViewModel> addQuestion,
            ObservableCollection<QuestionViewModel> listQuestionViewModel)
        {
            _windowManager = windowManager;
            _addQuestion = addQuestion;
            _userList = _userList.GetAllQuestions();
            _listQuestionViewModel = listQuestionViewModel;
        }

        public ObservableCollection<UserViewModel> UserList
        {
            get { return _userList; }
            set
            {
                _userList = value;
                NotifyOfPropertyChange(() => UserList);
            }
        }
        public ObservableCollection<QuestionViewModel> ListQuestionViewModel
        {
            get { return _listQuestionViewModel; }
            set { _listQuestionViewModel = value; NotifyOfPropertyChange(() => ListQuestionViewModel); }
        }

        public void AddQuestion() => _windowManager.ShowDialog(_addQuestion(_listQuestionViewModel));

        public void DeleteQuestion()
        {
            var list = _listQuestionViewModel.ToList();
            list.RemoveAll(x => x.IsCheked == true);
            _listQuestionViewModel.Clear();

            foreach(var q in list)
            {
                _listQuestionViewModel.Add(q);
            }

            _listQuestionViewModel.Save();
        }

        public void DeleteUser()
        {
            var list = _userList.ToList();
            list.RemoveAll(x => x.IsCheked == true);
            _userList.Clear();

            foreach (var q in list)
            {
                _userList.Add(q);
            }
            
            _userList.Save();
        }

    }
}
