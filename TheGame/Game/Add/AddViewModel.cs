﻿using Caliburn.Micro;
using Game.Models;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Add
{
    public class AddViewModel : Screen
    {
        private ObservableCollection<QuestionViewModel> _listQuestionViewModel;
        private QuestionViewModel _questionModel;
        private object _lock;
        private string _image;

        public AddViewModel (ObservableCollection<QuestionViewModel> listQuestionViewModel)
        {
            DisplayName = "Add Question";
            _lock = new object();
            _image = null;
            _listQuestionViewModel = listQuestionViewModel;
            _questionModel = new QuestionViewModel();
            _questionModel.Answer = new List<RsponseViewModel>();
            _questionModel.PossibleAnswer1 = new RsponseViewModel();
            _questionModel.PossibleAnswer2 = new RsponseViewModel();
            _questionModel.PossibleAnswer3 = new RsponseViewModel();
            _questionModel.PossibleAnswer4 = new RsponseViewModel();
        }

        public string Image
        {
            get { return _image; }
            set
            {
                _image = value;
                NotifyOfPropertyChange(() => Image);
            }
        }

        public QuestionViewModel QuestionModel
        {
            get { return _questionModel; }
            set { _questionModel = value; NotifyOfPropertyChange(() => QuestionModel); }
        }
        public void Add()
        {
            _listQuestionViewModel.Add(_questionModel);
            _listQuestionViewModel.Save();
            TryClose();
        }

        public void AddImage()
        {
            OpenFileDialog open = new OpenFileDialog();
            open.ShowDialog();
            _questionModel.Images = open.FileName;
            Image = open.FileName;

        }
    }
}
