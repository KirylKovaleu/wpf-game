﻿using Caliburn.Micro;
using Game.Models;
using System.Collections.ObjectModel;
using System.Linq;

namespace Game.Authorize
{
    public class AuthorizeViewModel : Screen
    {
        private UserViewModel _userModel;

        private ObservableCollection<UserViewModel> _userListModel;

        public AuthorizeViewModel(UserViewModel userModel)
        {
            _userModel = new UserViewModel();
            _userListModel = _userListModel.GetAllQuestions();
            if(_userListModel.Count == 0)
            {
                Inicialize();
                _userListModel = _userListModel.GetAllQuestions();
            }

            _userModel = Authorize(userModel);
        }
        
        public UserViewModel UserModel
        {
            get { return _userModel; }
            set
            {
                _userModel = value;
                NotifyOfPropertyChange(() => UserModel);
            }
        }
        public UserViewModel Authorize(UserViewModel userModel)
        {
            userModel.login = userModel.login.ToLower();
            userModel.Password = userModel.Password.ToLower();

            var user = _userListModel.GetAllQuestions().FirstOrDefault(x => x.login.ToLower().Equals(userModel.login));
            
            if(user != null)
            {
                var isResult = user.Password.ToLower().Equals(userModel.Password);
                if (isResult)
                {
                    _userModel = user;
                    return user;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public void Inicialize()
        {
            var users = new ObservableCollection<UserViewModel>
            {
                new UserViewModel{ Age=8, Name="Родион", Password="Родиошка", login="Родион", UserRole = new RoleViewModel{ Role = Role.User} },
                new UserViewModel{ Age=32, Name="Диана", Password="12345", login="Диана", UserRole = new RoleViewModel{ Role = Role.User} },
                new UserViewModel{ Age=32, Name="Кирилл", Password="Мой Хозяин", login="Кирилл", UserRole = new RoleViewModel{ Role = Role.Admin} }
            };

            users.Save();
        }
    }
}
