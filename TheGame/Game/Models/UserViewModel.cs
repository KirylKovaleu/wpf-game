﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Models
{
    public class UserViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string login { get; set; }
        public string Password { get; set; }
        public int Age { get; set; }
        public bool IsCheked { get; set; }
        public RoleViewModel UserRole { get; set; }
    }
}
