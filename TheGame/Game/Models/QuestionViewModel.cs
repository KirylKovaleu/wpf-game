﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Game.Models
{
    public class QuestionViewModel
    {
        private int _number;
        private string _questionn;
        private List<RsponseViewModel> _answer;
        private RsponseViewModel _possibleAnswer1;
        private RsponseViewModel _possibleAnswer2;
        private RsponseViewModel _possibleAnswer3;
        private RsponseViewModel _possibleAnswer4;
        private string _answerToUser;
        private string _answerTrue;
        private bool isCheked;
        private string images;
        
        public string Images
        {
            get { return images; }
            set { images = value; }
        }
        public bool IsCheked
        {
            get { return isCheked; }
            set { isCheked = value; }
        }
        public int AgeUser
        {
            get { return _number; }
            set
            {
                _number = value;
            }
        }
        public string Questionn
        {
            get { return _questionn; }
            set {
                _questionn = value;
                }
        }
        public List<RsponseViewModel> Answer
        {
            get { return _answer; }
            set { _answer = value; }
        }
        public RsponseViewModel PossibleAnswer1
        {
            get { return _possibleAnswer1; }
            set
            {
                _possibleAnswer1 = value;
            }
        }
        public RsponseViewModel PossibleAnswer2
        {
            get { return _possibleAnswer2; }
            set
            {
                _possibleAnswer2 = value;
            }
        }
        public RsponseViewModel PossibleAnswer3
        {
            get { return _possibleAnswer3; }
            set
            {
                _possibleAnswer3 = value;
            }
        }
        public RsponseViewModel PossibleAnswer4
        {
            get { return _possibleAnswer4; }
            set
            {
                _possibleAnswer4 = value;
            }
        }
        public string AnswerToUser
        {
            get { return _answerToUser; }
            set
            {
                _answerToUser = value;
            }
        }
        public string AnswerTrue
        {
            get { return _answerTrue; }
            set
            {
                _answerTrue = value;
            }
        }
    }
}
