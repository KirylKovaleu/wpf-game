﻿namespace Game.Models
{
    public class RsponseViewModel
    {
        private bool isCheck;
        private string _text;

        public bool IsCheck
        {
            get { return isCheck; }
            set
            {
                isCheck = value;
            }
        }

        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
            }
        }
    }
}
