﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Models
{
    public class RoleViewModel
    {
        public Guid Id { get; set; }
        public Role Role { get; set; }
    }
}
