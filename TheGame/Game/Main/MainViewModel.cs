﻿using Autofac;
using Caliburn.Micro;
using Game.AdminPanel;
using Game.Authorize;
using Game.Models;
using Game.NewQuestion;
using Game.Registration;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

namespace Game.Main
{

    public class MainViewModel : Screen, IDataErrorInfo
    {
        private QuestionViewModel _questionModel;
        private UserViewModel _userModel;
        private string _errorMessage;
        private string _password;
        private int _age;
        private readonly IWindowManager _windowManager;
        private ObservableCollection<QuestionViewModel> _listQuestionViewModel;
        private Func<QuestionViewModel, string, ShowQuestionViewModel> _showQuestionViewModel;
        private Func<ObservableCollection<QuestionViewModel>, AdminViewModel> _adminViewModel;
        private Func<UserViewModel, AuthorizeViewModel> _authorizeModel;
        private Func<RegistrationViewModel> _registrModel;

        public MainViewModel(IWindowManager windowManager,
            Func<QuestionViewModel, string, ShowQuestionViewModel> showQuestionViewModel,
            Func<ObservableCollection<QuestionViewModel>, AdminViewModel> adminViewModel,
            Func<UserViewModel, AuthorizeViewModel> authorizeModel,
            Func<RegistrationViewModel> registrModel)
        {
            _password = null;
            _errorMessage = null;
            _userModel = new UserViewModel();
            _windowManager = windowManager;
            _adminViewModel = adminViewModel;
            _authorizeModel = authorizeModel;
            _registrModel = registrModel;
            _showQuestionViewModel = showQuestionViewModel;
            _questionModel = new QuestionViewModel();
            _listQuestionViewModel = _listQuestionViewModel.GetAllQuestions();
        }

        public UserViewModel UserModel
        {
            get { return _userModel; }
            set
            {
                _userModel = value;
                NotifyOfPropertyChange(() => UserModel);
            }
        }
        public string this[string columnName]
        {
            get
            {
                string error = string.Empty;
                switch (columnName)
                {
                    case "Age":
                        {
                            if ((Age <= 0) || (Age > 100))
                            {
                                error = "Возраст должен быть больше 0 и меньше 100";
                            }
                            break;
                        }
                }
                return error;
            }
        }
        public int Age
        {
            get { return _age; }
            set
            {
                _age = value;
                //NotifyOfPropertyChange(() => Age);
            }
        }
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                NotifyOfPropertyChange(() => Password);
            }
        }
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; NotifyOfPropertyChange(() => ErrorMessage); }
        }
        public QuestionViewModel QuestionModel
        {
            get { return _questionModel; }
            set
            {
                _questionModel = value;
                NotifyOfPropertyChange(() => QuestionModel);
            }
        }

        public ObservableCollection<QuestionViewModel> ListQuestionViewModel
        {
            get { return _listQuestionViewModel; }
            set
            {
                _listQuestionViewModel = value;
                NotifyOfPropertyChange(() => ListQuestionViewModel);
            }
        }

        public string Error => throw new NotImplementedException();
        
        public void StartGame()
        {
            var coutTrue = 0;
            var countQuest = 0;


            if (_userModel.Age != 0)
            {

                    bool result = true;
                    
                    foreach (var quest in GetQuestions(_userModel.Age))
                    {

                        countQuest++;

                        if (result == false)
                        {
                            if ((bool)_windowManager.ShowDialog(_showQuestionViewModel(quest, "Это правильный ответ молодец")))
                            {
                                coutTrue++;
                            }
                            else
                            {
                                _windowManager.ShowDialog(_showQuestionViewModel(null, quest.AnswerToUser));
                                break;
                            }
                        }

                        else 
                        {
                            if (result && (bool)_windowManager.ShowDialog(_showQuestionViewModel(quest, "Хорошо, тогда давай начинать")))
                            {
                                coutTrue++;
                                result = false;
                            }
                            else
                            {
                                _windowManager.ShowDialog(_showQuestionViewModel(null, quest.AnswerToUser));
                                break;
                            }
                        }
                        
                    }

                    if (coutTrue == countQuest && _userModel.Age <= 12)
                    {
                        Process.Start("F:/Games/Mafia III/mafia3.exe");
                        countQuest = 0;
                        coutTrue = 0;
                    }

                if (coutTrue == countQuest && _userModel.Age > 12)
                {
                    Process.Start("https://ok.ru/?_erv=viewlyirbwpynedra");
                    countQuest = 0;
                    coutTrue = 0;
                }
            }
            else
            {
                _errorMessage = "Логин или пароль введены не верно";
                Refresh();
            }
        }

        public void Authorize()
        {
            var user = _authorizeModel(_userModel);

            if(user.UserModel != null)
            {

                if(user.UserModel.UserRole.Role == Role.Admin)
                {
                    _windowManager.ShowDialog(_adminViewModel(_listQuestionViewModel));
                }
                else
                {
                    _errorMessage = "Добро пожаловать " + user.UserModel.login + ",\n теперь вы можете играть";
                    _userModel.login = null;
                    _userModel.Password = null;
                    Refresh();
                    _userModel.Age = user.UserModel.Age;
                }
            }
        }

        public void Registration() => _windowManager.ShowDialog(_registrModel());

        public List<QuestionViewModel> GetQuestions(int age)
        {
            if(age <= 12)
            {
                var questions = _listQuestionViewModel.ToList().FindAll(x => x.AgeUser <= 12);
                Random r = new Random();

                for (var i = 0; i < questions.Count(); i++)
                {
                    int j = r.Next(i);
                    var temp = questions[i];
                    questions[i] = questions[j];
                    questions[j] = temp;

                }

                return questions;
            }
            else
            {
                var questions = _listQuestionViewModel.ToList().FindAll(x => x.AgeUser > 12);
                Random r = new Random();

                for (var i = 0; i < questions.Count(); i++)
                {
                    int j = r.Next(i);
                    var temp = questions[i];
                    questions[i] = questions[j];
                    questions[j] = temp;

                }

                return questions;
            }
        }

    }
}
