﻿using Autofac;
using Caliburn.Micro;
using Game.Main;
using Game.NewQuestion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace Game
{
    class MyBootstrapper : BootstrapperBase
    {
        private Autofac.IContainer _container;
        protected Autofac.IContainer Container
        {
            get { return _container; }
        }

        public MyBootstrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyTypes(AssemblySource.Instance.ToArray())
              .Where(type => type.Name.EndsWith("ViewModel"))
              .Where(type => type.GetInterface(typeof(INotifyPropertyChangedEx).Name) != null)
              .AsSelf()
              .InstancePerDependency();

            builder.RegisterAssemblyTypes(AssemblySource.Instance.ToArray())
              .Where(type => type.Name.EndsWith("View"))
              .Where(type => !string.IsNullOrWhiteSpace(type.Namespace) && type.Namespace.EndsWith("Views"))
              .AsSelf()
              .InstancePerDependency();

            builder.Register<IWindowManager>(c => new WindowManager()).InstancePerLifetimeScope();
            builder.Register<IEventAggregator>(c => new EventAggregator()).InstancePerLifetimeScope();

            ConfigureContainer(builder);

            _container = builder.Build();

            ConfigureViewLocator();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<MainViewModel>();
        }

        protected override async void OnExit(object sender, EventArgs e)
        {
             base.OnExit(sender, e);
        }

        protected override void BuildUp(object instance)
        {
            Container.InjectProperties(instance);
        }

        protected override object GetInstance(Type service, string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                if (Container.IsRegistered(service))
                {
                    return Container.Resolve(service);
                }
                else if (Container.IsRegisteredWithName(key, service))
                {
                    Container.ResolveNamed(key, service);
                }
            }

            throw new Exception(string.Format("Could not locate any instances of contract {0}.", key ?? service.Name));
        }

        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return Container.Resolve(typeof(IEnumerable<>).MakeGenericType(serviceType)) as IEnumerable<object>;
        }

        protected override IEnumerable<Assembly> SelectAssemblies()
        {
            return new[]
            {
                Assembly.GetExecutingAssembly()
            };
        }

        protected virtual void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterType<ShowQuestionViewModel>().AsSelf();
            //builder.RegisterType<HttpClient>().AsSelf();
        }

        private static void ConfigureViewLocator()
        {
            var oldViewLocator = ViewLocator.LocateForModelType;
            ViewLocator.LocateForModelType = (type, o, arg3) =>
            {
                string viewTypeName = type.Name.Substring(0, type.Name.LastIndexOf("Model"));

                var viewType = AppDomain.CurrentDomain.GetAssemblies()
                    .Select(a => a.GetTypes())
                    .Select(x => x.FirstOrDefault(t => t.Name == viewTypeName))
                    .FirstOrDefault(x => x != null);

                return ViewLocator.GetOrCreateViewType(viewType);
            };
        }
    }
}
