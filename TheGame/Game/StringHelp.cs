﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Game
{
    public static class StringHelp
    {
        public static char WriteString(this string str)
        {
            string s1 = str;
            foreach (var s in s1)
            {
                Thread.Sleep(70);
                return s;
            }
            return ' ';
        }
    }
}
